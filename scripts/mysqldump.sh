#!/bin/bash
now=$(date +"%Y-%m-%d.%H:%M:%S")
sudo mkdir -p /var/backup
sudo chmod -R 777 /var/backup
sudo rm -f /var/backup/smp.sql.gz
sudo mysqldump -u root -p1234 smp > /var/backup/smp.sql
sudo gzip -9 /var/backup/smp.sql
sudo aws s3 cp /var/backup/smp.sql.gz s3://specialmetals/db_backups/smp_db_backup_$now.sql.gz
