<?php
namespace AppBundle\Service;

use Snc\RedisBundle\Doctrine\Cache\RedisCache;
use Predis\Client;
use AppBundle\Service\Cache;

final class PredisClient implements Cache
{
	private $predis;

	public function __construct()
	{
		$this->predis = new RedisCache();
		$this->predis->setRedis(new Client());
	}

	public function init()
	{
		return $this->predis;
	}

	public function flushAll()
	{
		$this->predis->flushAll();
	}
}
