<?php
namespace AppBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StaticPageController extends Controller
{
	/**
     * @Route("/{page}/", name="frontend_static_page")
     */
	public function indexAction($page)
	{
    	try {
    		if ($page == "barrasdetitanio.php" || $page == "barrasdetitanio") {
				return $this->redirect('http://www.smp.es/material/barras-de-titanio-varillas-de-titanio-discos-de-titanio/', 301);
			}

			if ($page == "titanio.php" || $page == "titanio") {
				return $this->redirect('http://www.smp.es/material/titanio/', 301);
			}

			if ($page == "carburodetungsteno.php" || $page == "carburodetungsteno") {
				return $this->redirect('http://www.smp.es/material/carburo-de-tungsteno-wolframio/', 301);
			}

			if ($page == "barrasdetungsteno.php" || $page == "barrasdetungsteno") {
				return $this->redirect('http://www.smp.es/material/barras-de-tungsteno/', 301);
			}

			if ($page == "tungsteno.php" || $page == "tungsteno") {
				return $this->redirect('http://www.smp.es/material/tungsteno-wolframio/', 301);
			}

			if ($page == "molibdeno.php" || $page == "molibdeno") {
				return $this->redirect('http://www.smp.es/material/molibdeno/', 301);
			}

			if ($page == "hilodetungsteno.php" || $page == "hilodetungsteno") {
				return $this->redirect('http://www.smp.es/material/hilo-de-tungsteno-espirales-de-tungsteno/', 301);
			}

			if ($page == "afinacion.php" || $page == "afinacion") {
				return $this->redirect('http://www.smp.es/material/afinacion-y-comercializacion-de-metales-preciosos/', 301);
			}

			if ($page == "colimadoresdetungsteno.php" || $page == "colimadoresdetungsteno") {
				return $this->redirect('http://www.smp.es/material/colimadores-de-tungsteno/', 301);
			}

			if ($page == "blancosparasputtering.php" || $page == "blancosparasputtering") {
				return $this->redirect('http://www.smp.es/material/blancos-targets-de-oro/', 301);
			}

			if ($page == "titanio.php" || $page == "titanio") {
				return $this->redirect('http://www.smp.es/material/titanio/', 301);
			}

			if ($page == "tantalio.php" || $page == "tantalio") {
				return $this->redirect('http://www.smp.es/material/tantalio/', 301);
			}

			if ($page == "planchasdetungsteno.php" || $page == "planchasdetungsteno") {
				return $this->redirect('http://www.smp.es/material/planchas-de-tungsteno-laminas-de-tungsteno/', 301);
			}

			if ($page == "anodosparaproteccioncatodica.php" || $page == "anodosparaproteccioncatodica") {
				return $this->redirect('http://www.smp.es/material/anodos-de-titanio/', 301);
			}

			if ($page == "niobio.php" || $page == "niobio") {
				return $this->redirect('http://www.smp.es/material/niobio/', 301);
			}

			if ($page == "electrodosdetungsteno.php" || $page == "electrodosdetungsteno") {
				return $this->redirect('http://www.smp.es/material/electrodos-de-tungsteno/', 301);
			}

			if ($page == "anodos.php" || $page == "anodos") {
				return $this->redirect('http://www.smp.es/material/anodos-de-titanio/', 301);
			}

			if ($page == "planchasdetitanio.php" || $page == "planchasdetitanio") {
				return $this->redirect('http://www.smp.es/material/planchas-de-titanio/', 301);
			}

			if ($page == "barrasdecarburodetungsteno.php" || $page == "barrasdecarburodetungsteno") {
				return $this->redirect('http://www.smp.es/material/barras-de-carburo-de-tungsteno-varillas-de-carburo-de-tungsteno/', 301);
			}
			
			return $this->render('AppBundle:Frontend:'.$page.'.html.twig');
		} catch (\Exception $e) {
		    return $this->render('Exception/error404.html.twig');
		}
	}
}
