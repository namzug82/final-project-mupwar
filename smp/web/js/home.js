var width = $(window).width()

if ((width >= 1023)) {
	$('.home-material-description').css("font-size", "16px", "font-family", "arial", "text-decoration", "none");
}

if ((width < 1023) && (width >= 768)) {
	$('.home-material-description').css("font-size", "22px", "font-family", "arial", "text-decoration", "none");
}

if ((width < 768) && (width >= 480)) {
	$('.home-material-description').css("font-size", "16px", "font-family", "arial", "text-decoration", "none");
}

if ((width < 480)) {
	$('.home-material-description').css("font-size", "14px", "font-family", "arial", "text-decoration", "none");
}

$('span').css("font-size", "18px", "font-family", "arial", "text-decoration", "none");
$( 'strong' ).contents().unwrap();